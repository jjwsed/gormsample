package db

import (
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

func init() {

	var err error
	db, err = gorm.Open("mysql", "root:f22873007@/bbsdb?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	db.LogMode(true)

	// Migrate the schema
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)
	db.DB().SetConnMaxLifetime(time.Hour)
	// defer db.Close()	//不知道什么时候关闭比较合适

	err = db.DB().Ping()
	if err != nil {
		panic("ping error")
	}

}

func NewDB() *gorm.DB {
	return db
}

func CloseDB(db *gorm.DB) {
	db.Close()
}
