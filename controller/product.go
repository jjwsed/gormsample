package controller

import (
	"fmt"
	"gormsample/db"

	"github.com/jinzhu/gorm"
)

type Product struct {
	gorm.Model
	Code  string
	Price uint
	Name  string
}

func (Product) TableName() string {
	return "product"
}

var dbx *gorm.DB

func init() {
	dbx = db.NewDB()
	dbx.AutoMigrate(&Product{})
}

func Close() {
	dbx.Close()
}

func ShowAll() {

	// db.AutoMigrate(&Product{})
	dbx.Create(&Product{Code: "L1212", Price: 1000, Name: "LLK"})

	// Read
	var product Product
	dbx.First(&product, 1) // find product with id 1
	fmt.Println(product, "haha")

}
