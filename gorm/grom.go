package main

import (
	"gormsample/controller"

	_ "github.com/go-sql-driver/mysql"
)

func main() {

	controller.ShowAll()

	controller.Close()

}
