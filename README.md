# gorm验证实例

本次通过第三方组建完成MySQL数据库的访问。


这里有一个比较特别的地方，在数据库连接部分，数据库是长连接的情况；

但是这里有一个问题， 即数据库关闭部分存在问题，暂时没有看到在什么地方关闭；

在controller层增加一个接口，用于进程关闭。

为什么不自己调用MySQL数据库的接口？采用gorm的接口呢？

> gorm可以实现 struct mapping to database SQL

## 支持数据库类型

两个数据库driver都比较多，支持如下：

gorm : MySQL, PostgreSQL, Sqlite3, 
xorm : MySQL, PostgreSQL, Sqlite3,  msSql, Oracle

## 支持struct 与 database的 mapping

支持从数据结构转化到建表语句

## 支持通用的查询语句

包括：

Where, SQL, Get, InSert, Delete等等；

## 对于DB层的方法

### 数据库管理

DB.Open(), DB.Close(), DB.New()

### 事务

 DB.Begin(), 开始事务、 DB.Commit(), 提交事务；

 ### 增删改查

 DB.Create, DB.Count, DB.Delete,  DB.Exec, DB.Find, DB.First, DB.FirstOrCreate, DB.FirstOrInit

 DB.Raw

 ### 表间关联

 DB.Joins


